package com.example.tp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.tp.R.id;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView listTeam;
    public static SwipeRefreshLayout swipe;

    SportDbHelper dbHelper;
    public static TeamRecyclerAdapter adapter;

    public static SportDbHelper DB;

    public static int nbrThreadsRunningRefresh = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        DB = new SportDbHelper(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                MainActivity.this.startActivityForResult(myIntent,1);
            }
        });

        dbHelper = new SportDbHelper(this);
        dbHelper.populate();

        this.linkFields();
        this.initFields();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK) {

            Team team = (Team) data.getParcelableExtra(Team.TAG);

            if (team != null) {
                DB.addTeam(team);
                Toast.makeText(this, "Team saved !", Toast.LENGTH_SHORT);
            } else {
                Toast.makeText(this, "The team is strange !", Toast.LENGTH_SHORT);
            }

        } else {
            Toast.makeText(this, "Something happened !", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {

        for (Team team : DB.getAllTeams()) {

            RefreshTeamContent runningTask = new RefreshTeamContent();
            runningTask.execute(team);

            nbrThreadsRunningRefresh++;
        }

    }

  // la liste s'affiche et on fait le swip pour que la maj se fait.
    private void linkFields() {
        this.listTeam = findViewById(R.id.list);
        swipe = findViewById(R.id.swipe);
    }

    private void initFields() {

        this.initList();

        // ici swip pour refresh
        swipe.setOnRefreshListener(this);

        //  retirer avec callback
        new ItemTouchHelper(removeCallback).attachToRecyclerView(listTeam);
    }

   // on va init ici la listview
    private void initList() {

        adapter = new TeamRecyclerAdapter(this);

        this.listTeam.setAdapter(adapter);
        listTeam.setLayoutManager(new LinearLayoutManager(this));

    }

    ItemTouchHelper.SimpleCallback removeCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            int position = viewHolder.getAdapterPosition();
            long id = DB.getAllTeams().get(position).getId();

            DB.deleteTeam(id);

            adapter.notifyDataSetChanged();
        }
    };

    private final class RefreshTeamContent extends AsyncTask<Team, Void, String> {

        @Override
        protected String doInBackground(Team... params) {

            Team team = params[0];

            // Prepare the team to be updated

            JSONResponseHandlerTeam responseHandlerTeam = new JSONResponseHandlerTeam(team);

            // Update image
            TeamActivity.loadTeamContent(team, responseHandlerTeam, MainActivity.this);

            // Update last event
            TeamActivity.loadTeamLastEvent(team, responseHandlerTeam, MainActivity.this);

            // TODO: HERE
            // https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=4430&s=1920
            // Update rank
            TeamActivity.loadTeamRank(team, responseHandlerTeam, MainActivity.this);

            // Save It into the DB
            DB.updateTeam(team);

            return TeamActivity.DONE;
        }

        @Override
        protected void onPostExecute(String result) {

            nbrThreadsRunningRefresh--;

            // If It's the last thread to finish It job
            if (nbrThreadsRunningRefresh <= 0) {

                // MAJ DE LA LISTVIEW
                // TODO : Fix doesn't work
                adapter.notifyDataSetChanged();

                swipe.setRefreshing(false);
            }
        }
    }
}
