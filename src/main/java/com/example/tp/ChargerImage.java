package com.example.tp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;

//cette page nous permet de recupérer l'image par rapport à la Team grace à l'URL.

public class ChargerImage extends AsyncTask<ImageView, Void, Bitmap> {

    ImageView imageView;
    Team team;

    public ChargerImage(ImageView imageView, Team team) {
        this.imageView = imageView;
        this.team = team;
    }

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {

        Bitmap bit = null;

        try {
            URL url = new URL(this.team.getTeamBadge());
            bit = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            Log.d("error_badge",e.toString());
        }

        return bit;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        imageView.setImageBitmap(result);

    }
}